using Roll;

namespace DarkFort;

public static class ClassAbilities
{
    public static string GetClassAbility(PlayerClass @class) => @class switch
    {
        PlayerClass.FangedDeserter => FangedDeserterAbilities[DiceRoller.RollDice(1, 6).Result - 1],
        PlayerClass.GutterbornScum => GutterbornScumAbilities[DiceRoller.RollDice(1, 6).Result - 1],
        PlayerClass.EsotericHermit => EsotericHermitAbilities[DiceRoller.RollDice(1, 6).Result - 1],
        PlayerClass.WretchedRoyalty => WretchedRoyaltyAbilities[DiceRoller.RollDice(1, 6).Result - 1],
        PlayerClass.HereticalPriest => HereticalPriestAbilities[DiceRoller.RollDice(1, 6).Result - 1],
        PlayerClass.OccultHerbmaster => OccultHerbmasterAbility,
        PlayerClass.Other => throw new InvalidOperationException(
            $"Cannot pass {nameof(PlayerClass.Other)} to {nameof(GetClassAbility)}"),
        _ => throw new ArgumentOutOfRangeException(nameof(@class), @class, null)
    };
    
    private static readonly string[] FangedDeserterAbilities = new string[]
    {
        "CRUMPLED MONSTER MASK",
        "THE BROWN SCMITAR OF GALGENBECK",
        "WIZARD TEETH",
        "OLD SIGûRD'S SLING",
        "ANCIENT GORE-HOUND",
        "THE SHOE OF DEATH'S HORSE",
    };

    private static readonly string[] GutterbornScumAbilities = new string[]
    {
        "COWARD'S JAB",
        "FILTHY FINGERSMITH",
        "ABOMINABLE GOB LOBBER",
        "ESCAPING FATE",
        "EXCRETAL STEALTH",
        "DODGING DEATH",
    };

    private static readonly string[] EsotericHermitAbilities = new string[]
    {
        "MASTER OF FATE",
        "BOOK OF BOILING BLOOD",
        "SPEAKER OF TRUTHS",
        "INITIATE OF THE INVISIBLE COLLEGE",
        "BARD OF THE UNDYING",
        "HAWK AS WEAPON",
    };

    private static readonly string[] WretchedRoyaltyAbilities = new string[]
    {
        "THE BLADE OF YOUR ANCESTORS",
        "'POLTROON' THE COURT JESTER",
        "BARBARISTER THE INCREDIBLE HORSE",
        "HAMFUND THE SQUIRE",
        "THE SNAKE-SKIN GIFT",
        "HORN OF THE SCHLESWIG LORDS",
    };

    private static readonly string[] HereticalPriestAbilities = new string[]
    {
        "SACRED SHEPHERD'S CROOK",
        "STOLEN MITRE",
        "LIST OF SINS",
        "THE BLASPHEMOUS NECHRUBEL BIBLE",
        "STONES TAKEN FROM THEL-EMAS' LOST TEMPLE",
        "CRUCIFIX",
    };

    private static readonly string OccultHerbmasterAbility = "Portable Laboratory";
}