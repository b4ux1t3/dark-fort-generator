using System.Text.Json.Serialization;
using Roll;

namespace DarkFort;

public enum PlayerClass
{
    Other,
    FangedDeserter,
    GutterbornScum,
    EsotericHermit,
    WretchedRoyalty,
    HereticalPriest,
    OccultHerbmaster,
}

public static class CharacterExtensions
{
    public static string ToClassString(this PlayerClass playerClass) => playerClass switch
    {
        PlayerClass.Other => "Other",
        PlayerClass.FangedDeserter => "Fanged Deserter",
        PlayerClass.GutterbornScum => "Gutterborn Scum",
        PlayerClass.EsotericHermit => "Esoteric Hermit",
        PlayerClass.WretchedRoyalty => "Wretched Royalty",
        PlayerClass.HereticalPriest => "Heretical Priest",
        PlayerClass.OccultHerbmaster => "Occult Herbmaster",
        _ => throw new ArgumentOutOfRangeException(nameof(playerClass), playerClass, null)
    };
}
/// <summary>
/// A Representation of a bare bones MÖRK BORG character.,
/// </summary>
public record Character
{
    public string Name { get; init; }
    public string Description { get; init; }

    [JsonIgnore]
    private PlayerClass PlayerClass { get; init; }

    public string Class => PlayerClass is PlayerClass.Other ? "Custom" : PlayerClass.ToClassString();

    public int HealthPoints { get; init; }
    public int Strength { get; init; }
    public int Agility { get; init; }
    public int Presence { get; init; }
    public int Toughness { get; init; }

    public int Silver { get; init; }
    
    public int Omens { get; init; }
    
    public int? Origin { get; init; }
    public string? Ability { get; init; }

    public static Character RandomCharacter() => _fromClass((PlayerClass) DiceRoller.RollDice(1, 6).Result, CharacterNames.GetName(), "");
    public static Character RandomCharacter(PlayerClass playerClass) => _fromClass(playerClass, CharacterNames.GetName(), "");

    public static Character RandomCharacter(PlayerClass playerClass, string name, string description = "") => 
        _fromClass(playerClass, 
            string.IsNullOrEmpty(name) ? CharacterNames.GetName() : name, 
            description
        );
    public static Character RandomCharacter(string name, string description = "") => 
        _fromClass(
            (PlayerClass) DiceRoller.RollDice(1, 6).Result, 
            string.IsNullOrEmpty(name) ? CharacterNames.GetName() : name, 
            description
        );
    
    private static Character _fromClass(PlayerClass playerClass, string name, string description)
    {
        return playerClass switch
        {
            PlayerClass.FangedDeserter => _fromFangedDeserter(name, description),
            PlayerClass.GutterbornScum => _fromGutterbornScum(name, description),
            PlayerClass.EsotericHermit => _fromEsotericHermit(name, description),
            PlayerClass.WretchedRoyalty => _fromWretchedRoyalty(name, description),
            PlayerClass.HereticalPriest => _fromHereticalPriest(name, description),
            PlayerClass.OccultHerbmaster => _fromOccultHerbmaster(name, description),
            PlayerClass.Other => _classlessCharacter(name, description),
            _ => throw new ArgumentOutOfRangeException(nameof(playerClass), playerClass, null)
        };
    }

    private static int _abilityScore(int rollSum) => rollSum switch
    {
        > 0 and <= 4 => -3,
        > 4 and <= 6 => -2,
        > 6 and <= 8 => -1,
        > 8 and <= 12 => 0,
        > 12 and <= 14 => 1,
        > 14 and <= 16 => 2,
        > 16 and <= 20 => 3,
        _ => throw new InvalidOperationException($"Range must be from 1 to 20. Given: {rollSum}")
    };

    private static Character _classlessCharacter(string name, string description)
    {
        var strengthRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var agilityRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var presenceRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var toughnessRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var hp = _abilityScore(toughnessRoll) + (int) DiceRoller.RollDice(1, 8).Result;
        var silver = (int) DiceRoller.RollDice(2, 6).Result * 10;
        var omens = (int) DiceRoller.RollDice(1, 2).Result;
        
        return new(
            name, 
            _abilityScore(strengthRoll), 
            _abilityScore(agilityRoll), 
            _abilityScore(presenceRoll), 
            _abilityScore(toughnessRoll), 
            silver, 
            int.Max(hp, 1), 
            omens,
            ability: null,
            origin: null,
            description
        );
    }

    private static Character _fromFangedDeserter(string name, string description)
    {
        var strengthRoll = (int) DiceRoller.RollDice(3, 6).Result + 2;
        var agilityRoll = (int) DiceRoller.RollDice(3, 6).Result - 1;
        var presenceRoll = (int) DiceRoller.RollDice(3, 6).Result - 1;
        var toughnessRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var hp = _abilityScore(toughnessRoll) + (int) DiceRoller.RollDice(1, 10).Result;
        var silver = (int) DiceRoller.RollDice(2, 6).Result * 10;
        var omens = (int) DiceRoller.RollDice(1, 2).Result;
        var ability = ClassAbilities.GetClassAbility(PlayerClass.FangedDeserter);
        var origin = (int) DiceRoller.RollDice(1, 6).Result;

        return new(
            name, 
            _abilityScore(strengthRoll), 
            _abilityScore(agilityRoll), 
            _abilityScore(presenceRoll), 
            _abilityScore(toughnessRoll), 
            silver, 
            int.Max(hp, 1), 
            omens,
            ability,
            origin,
            description,
            PlayerClass.FangedDeserter
        );
    }

    private static Character _fromGutterbornScum(string name, string description)
    {
        var strengthRoll = (int) DiceRoller.RollDice(3, 6).Result - 2;
        var agilityRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var presenceRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var toughnessRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var hp = _abilityScore(toughnessRoll) + (int) DiceRoller.RollDice(1, 6).Result;
        var silver = (int) DiceRoller.RollDice(1, 6).Result * 10;
        var omens = (int) DiceRoller.RollDice(1, 2).Result;
        var ability = ClassAbilities.GetClassAbility(PlayerClass.GutterbornScum);
        var origin = (int) DiceRoller.RollDice(1, 6).Result;

        return new(
            name, 
            _abilityScore(strengthRoll), 
            _abilityScore(agilityRoll), 
            _abilityScore(presenceRoll), 
            _abilityScore(toughnessRoll), 
            silver, 
            int.Max(hp, 1), 
            omens,
            ability,
            origin,
            description,
            PlayerClass.GutterbornScum
        );
    }

    private static Character _fromEsotericHermit(string name, string description)
    {
        var strengthRoll = (int) DiceRoller.RollDice(3, 6).Result - 2;
        var agilityRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var presenceRoll = (int) DiceRoller.RollDice(3, 6).Result + 2;
        var toughnessRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var hp = _abilityScore(toughnessRoll) + (int) DiceRoller.RollDice(1, 4).Result;
        var silver = (int) DiceRoller.RollDice(1, 6).Result * 10;
        var omens = (int) DiceRoller.RollDice(1, 4).Result;
        var ability = ClassAbilities.GetClassAbility(PlayerClass.EsotericHermit);
        var origin = (int) DiceRoller.RollDice(1, 6).Result;

        return new(
            name, 
            _abilityScore(strengthRoll), 
            _abilityScore(agilityRoll), 
            _abilityScore(presenceRoll), 
            _abilityScore(toughnessRoll), 
            silver, 
            int.Max(hp, 1), 
            omens,
            ability,
            origin,
            description,
            PlayerClass.EsotericHermit
        );
    }

    private static Character _fromWretchedRoyalty(string name, string description)
    {
        var hp = (int) DiceRoller.RollDice(1, 6).Result;
        var silver = (int) DiceRoller.RollDice(4, 6).Result * 10;
        var omens = (int) DiceRoller.RollDice(1, 2).Result;
        var ability = ClassAbilities.GetClassAbility(PlayerClass.WretchedRoyalty);
        var origin = (int) DiceRoller.RollDice(1, 6).Result;

        return new(
            name, 
            0,
            0,
            0,
            0,
            silver, 
            int.Max(hp, 1), 
            omens,
            ability,
            origin,
            description,
            PlayerClass.WretchedRoyalty
        );
    }

    private static Character _fromHereticalPriest(string name, string description)
    {
        var strengthRoll = (int) DiceRoller.RollDice(3, 6).Result - 2;
        var agilityRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var presenceRoll = (int) DiceRoller.RollDice(3, 6).Result + 2;
        var toughnessRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var hp = _abilityScore(toughnessRoll) + (int) DiceRoller.RollDice(1, 8).Result;
        var silver = (int) DiceRoller.RollDice(3, 6).Result * 10;
        var omens = (int) DiceRoller.RollDice(1, 4).Result;
        var ability = ClassAbilities.GetClassAbility(PlayerClass.HereticalPriest);
        var origin = (int) DiceRoller.RollDice(1, 6).Result;

        return new(
            name, 
            _abilityScore(strengthRoll), 
            _abilityScore(agilityRoll), 
            _abilityScore(presenceRoll), 
            _abilityScore(toughnessRoll), 
            silver, 
            int.Max(hp, 1), 
            omens,
            ability,
            origin,
            description,
            PlayerClass.HereticalPriest
        );
    }

    private static Character _fromOccultHerbmaster(string name, string description)
    {
        var strengthRoll = (int) DiceRoller.RollDice(3, 6).Result - 2;
        var agilityRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var presenceRoll = (int) DiceRoller.RollDice(3, 6).Result;
        var toughnessRoll = (int) DiceRoller.RollDice(3, 6).Result + 2;
        var hp = _abilityScore(toughnessRoll) + (int) DiceRoller.RollDice(1, 6).Result;
        var silver = (int) DiceRoller.RollDice(2, 6).Result * 10;
        var omens = (int) DiceRoller.RollDice(1, 2).Result;
        var ability = ClassAbilities.GetClassAbility(PlayerClass.OccultHerbmaster);
        var origin = (int) DiceRoller.RollDice(1, 6).Result;

        return new(
            name, 
            _abilityScore(strengthRoll), 
            _abilityScore(agilityRoll), 
            _abilityScore(presenceRoll), 
            _abilityScore(toughnessRoll), 
            silver, 
            int.Max(hp, 1), 
            omens,
            ability,
            origin,
            description,
            PlayerClass.OccultHerbmaster
        );
    }

    public Character(string name, int strength, int agility, int presence, int toughness, int silver, int hp, int omens, string? ability, int? origin,
        string description = "", PlayerClass playerClass = PlayerClass.Other)
    {
        Strength = strength;
        Agility = agility;
        Presence = presence;
        Name = name;
        Toughness = toughness;
        Silver = silver;
        Description = description;
        HealthPoints = hp;
        Omens = omens;
        Ability = ability;
        Origin = origin;
        PlayerClass = playerClass;
    }
}