namespace DarkFort;

public record Ability(string Name, string Description);