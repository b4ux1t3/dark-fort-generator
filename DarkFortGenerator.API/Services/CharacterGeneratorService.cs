using DarkFort;

namespace DarkFortGenerator.API.Services;

public class CharacterGeneratorService
{
    public Character GenerateRandomCharacter(PlayerClass playerClass = PlayerClass.Other, string name = "", string description = "")
    {
        return playerClass is PlayerClass.Other ? _generateRandomClasslessCharacter(name, description) : _generatedRandomCharacter(playerClass, name, description);
    }

    public Character GenerateRandomClassCharacter(string name, string description) => Character.RandomCharacter(name, description);
    private Character _generateRandomClasslessCharacter( string name = "", string description = "") => Character.RandomCharacter(PlayerClass.Other, name, description);
    private Character _generatedRandomCharacter(PlayerClass playerClass = PlayerClass.Other, string name = "", string description = "") => Character.RandomCharacter(playerClass, name, description);
    
}