using DarkFort;

namespace DarkFortGenerator.API.Data;

/// <summary>
/// A record describing the information needed to generate a randomized character.
/// </summary>
/// <param name="Name">The name for the new character.</param>
/// <param name="Description">The description for the new character.</param>
/// <param name="PlayerClass">An optional player class. If left empty, and <see cref="RandomClass"/> is false, a randomized custom character will be generated.</param>
/// <param name="RandomClass">Whether you want a random class. False by default.</param>
public record CharacterCreationRequest(string? Name, string? Description, PlayerClass? PlayerClass, bool RandomClass = false);