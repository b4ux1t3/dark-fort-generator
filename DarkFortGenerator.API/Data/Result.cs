namespace DarkFortGenerator.API.Data;

/// <summary>
/// An implementation of the result pattern. 
/// </summary>
/// <param name="Value">The actual value that results from a request. This value should be null if there is an error.</param>
/// <param name="Error">An error message which represents something that went wrong during the processing of a request. This value should be null if there is a value.</param>
public record Result<T>(T? Value, Error? Error)
{
    /// <summary>
    /// True if the error property is set.
    /// </summary>
    public bool IsError => Error is not null;
}
/// <summary>
/// A simple error message object. 
/// </summary>
/// <param name="Message"></param>
public record Error(string Message);

