using DarkFort;

namespace DarkFortGenerator.API.Data;

/// <summary>
/// For accessing characters in a database
/// </summary>
public interface ICharacterRepository
{
    /// <summary>
    /// Returns a character from a given GUID
    /// </summary>
    /// <param name="id">A GUID to search for</param>
    public Result<Character> GetCharacter(Guid id);
    /// <summary>
    /// Returns all characters currently in the database
    /// </summary>
    public Result<List<CharacterEntity>> GetAllCharacters();
    /// <summary>
    /// Creates a <see cref="CharacterEntity"/> from the provided <see cref="Character"/> and stores it in the database.
    /// </summary>
    /// <param name="character">A character for which to create a database record</param>
    /// <returns>The resulting database entity</returns>
    public Result<CharacterEntity> CreateCharacter(Character character);
}

