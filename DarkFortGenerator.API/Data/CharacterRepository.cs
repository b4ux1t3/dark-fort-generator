using DarkFort;

namespace DarkFortGenerator.API.Data;

/// <summary>
/// An implementation of <see cref="ICharacterRepository"/> which uses a simple, in-memory dictionary to store the characters.
/// </summary>
public class CharacterRepository : ICharacterRepository
{
    private readonly Dictionary<Guid, CharacterEntity> _entities = new();

    /// <inheritdoc />
    public Result<Character> GetCharacter(Guid id)
    {
        var success = _entities.TryGetValue(id, out var entity);

        if (success && entity is not null) return new Result<Character>(entity.Character, null);
        return new Result<Character>(null, new($"Unable to find character with ID {id}"));
    }

    /// <inheritdoc />
    public Result<List<CharacterEntity>> GetAllCharacters()
    {
        return new(_entities.Select(k => k.Value).ToList(), null);
    }

    /// <inheritdoc />
    public Result<CharacterEntity> CreateCharacter(Character character)
    {
        var newGuid = Guid.NewGuid();
        
        _entities[newGuid] = new CharacterEntity(newGuid, character);

        return new (_entities[newGuid], null);
    }
}

/// <summary>
/// A representation of a <see cref="Character"/> which is tied to a unique ID (<see cref="Id"/>)
/// </summary>
/// <param name="Id">The unique ID for this character</param>
/// <param name="Character">The character tied to this record</param>
public record CharacterEntity(Guid Id, Character Character);
