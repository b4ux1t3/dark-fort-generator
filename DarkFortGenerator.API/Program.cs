using System.Reflection;
using System.Text.Json.Serialization;
using DarkFortGenerator.API;
using DarkFortGenerator.API.Data;
using DarkFortGenerator.API.Services;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.OpenApi.Models;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers().AddJsonOptions(x =>
{
    // serialize enums as strings in api responses (e.g. Role)
    x.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
});;
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
    {
        options.SwaggerDoc("v1", new OpenApiInfo
        {
            Version = "v1",
            Title = "Dark Fort Generator",
            Description = @"A Web API for generating MÖRK BORG characters.

Dark Fort Generator is an independent production by Chris Pilcher and is not affiliated with Ockult Örtmästare Games or Stockholm Kartell. It is published under the MÖRK BORG Third Party License.

MÖRK BORG is copyright Ockult Örtmästare Games and Stockholm Kartell.",
            License = new OpenApiLicense
            {
                Name = "MIT License",
                Url = new Uri("https://gitlab.com/b4ux1t3/dark-fort-generator/-/blob/main/LICENSE")
            },
            Contact = new OpenApiContact
            {
                Email = "dfg@bauxite.tech"
            }
        });

        // using System.Reflection;
        var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
    }
);

builder.Services.TryAddSingleton<ICharacterRepository, CharacterRepository>();
builder.Services.TryAddTransient<CharacterGeneratorService>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
}
app.UseSwagger();
app.UseSwaggerUI();

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();