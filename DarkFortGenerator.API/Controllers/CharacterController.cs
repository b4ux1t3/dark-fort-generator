using DarkFort;
using DarkFortGenerator.API.Data;
using Microsoft.AspNetCore.Mvc;

namespace DarkFortGenerator.API.Controllers;

[ApiController]
[Route($"[controller]")]
public class CharacterController : ControllerBase
{
    private readonly ILogger<CharacterController> _logger;
    private readonly ICharacterRepository _characterRepository;

    public CharacterController(ILogger<CharacterController> logger, ICharacterRepository characterRepository)
    {
        _logger = logger;
        _characterRepository = characterRepository;
    }

    [HttpGet("id/{id:guid}")]
    public Result<Character> GetCharacterById(Guid id) =>_characterRepository.GetCharacter(id);
    

    [HttpGet("all")]
    public Result<List<CharacterEntity>> GetAllCharacters() => _characterRepository.GetAllCharacters();
    
}