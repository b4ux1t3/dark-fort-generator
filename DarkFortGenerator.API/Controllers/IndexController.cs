using DarkFortGenerator.API.Data;
using Microsoft.AspNetCore.Mvc;

namespace DarkFortGenerator.API.Controllers;

/// <summary>
/// Handles the root path of our API
/// </summary>
[ApiController]
[Route($"/")]
public class IndexController : ControllerBase
{
    private readonly ILogger<IndexController> _logger;
    private readonly ICharacterRepository _characterRepository;

    /// <inheritdoc />
    public IndexController(ILogger<IndexController> logger, ICharacterRepository characterRepository)
    {
        _logger = logger;
        _characterRepository = characterRepository;
    }

    /// <summary>
    /// Returns a short message to help developers find the actual tool.
    /// </summary>
    [HttpGet]
    public ContentResult Get()
    {
        return new ContentResult 
        {
            ContentType = "text/html",
            Content = "<div>There's nothing at the root here. Check the <a href=\"/swagger/index.html\">docs.</a></div>"
        };
    }
}