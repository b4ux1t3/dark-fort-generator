using DarkFort;
using DarkFortGenerator.API.Data;
using DarkFortGenerator.API.Services;
using Microsoft.AspNetCore.Mvc;

namespace DarkFortGenerator.API.Controllers;

/// <summary>
/// The controller for creating new characters
/// </summary>
[ApiController]
[Route($"[controller]")]
public class CharacterGeneratorController : ControllerBase
{
    private readonly ILogger<CharacterGeneratorController> _logger;
    private readonly CharacterGeneratorService _service;
    private readonly ICharacterRepository _characterRepository;

    /// <inheritdoc />
    public CharacterGeneratorController(ILogger<CharacterGeneratorController> logger, CharacterGeneratorService service, ICharacterRepository characterRepository)
    {
        _logger = logger;
        _service = service;
        _characterRepository = characterRepository;
    }

    /// <summary>
    /// Create a new character and save it to the database.
    /// </summary>
    /// <param name="creationRequest">A record describing the information needed to generate a randomized character.</param>
    /// <returns>A <see cref="Result{T}"/> containing a <see cref="CharacterEntity"/> with the randomly-created character.</returns>
    [HttpPost]
    public Result<CharacterEntity> CreateCharacter([FromBody] CharacterCreationRequest creationRequest)
    {
        var name = creationRequest.Name ?? "";
        var description = creationRequest.Description ?? "";
        
        if (creationRequest.RandomClass)
        {
            var newClassChar =_service.GenerateRandomClassCharacter(name, description);
            _logger.LogInformation("New Character created {}", newClassChar);
            return _characterRepository.CreateCharacter(newClassChar);
        }
        
        var newChar = _service.GenerateRandomCharacter(creationRequest.PlayerClass ?? PlayerClass.Other, name, description);
        _logger.LogInformation("New Character created {}", newChar);
        return _characterRepository.CreateCharacter(newChar);
    }
}